﻿/*
 * @author      Mariusz Jakubowski mariusz@orion.gd
 */

using UnityEngine;
using System.Collections;

public class GoldMine : MonoBehaviour {

    // Set it in object inspector
    public float  goldPerHour = 6000f;

    private float startProductionTime;          // Real time start of production
    private float goldProduced;                 // Counter gold produced
    private float safe;                         // Produced - spend
    private float shouldbeGoldProduced;         // Calculated number of gold should be prodeced

    void Start ()
    {
        startProductionTime = Time.realtimeSinceStartup;
    }
	
	void FixedUpdate ()
    {
        shouldbeGoldProduced = (Time.realtimeSinceStartup - startProductionTime) / (3600 / goldPerHour);

        if (shouldbeGoldProduced > goldProduced && Time.timeScale > 0)
        {
            goldProduced++;
            safe++;
        }
    }

    public bool payment(float _price)
    {
        safe -= _price;

        if (safe < 0)
        {
            safe = 0;
            return false;
        }

        return true;
    }

    void OnGUI()
    {
        Vector2 textLocation = Camera.main.WorldToScreenPoint(transform.position);
        GUI.Box(new Rect(textLocation.x - 35, Screen.height - textLocation.y - 50, 70, 25), safe.ToString() + " gold");
    }
}
