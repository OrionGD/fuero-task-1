﻿/*
 * @author      Mariusz Jakubowski mariusz@orion.gd
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cam : MonoBehaviour {

	public void ChangeTimeScale()
    {
        Text pauseButtonText = GameObject.Find("PauseButtonText").GetComponent<Text>();

        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            pauseButtonText.text = "Pause";
        }
        else
        {
            Time.timeScale = 0;
            pauseButtonText.text = "Resume";
        }
    }
}
