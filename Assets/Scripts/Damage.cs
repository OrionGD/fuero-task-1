﻿/*
 * @author      Mariusz Jakubowski mariusz@orion.gd
 */

using UnityEngine;
using System.Collections;

public class Damage : MonoBehaviour {

    public float health = 50;
    public float armor = 2;
    public float atackDamage = 10;
    public int   reloadTime = 30;
	

    public void ReceiveDamage(float _power)
    {
        health -= _power - armor;
    }
}
