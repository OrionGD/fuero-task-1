﻿/*
 * @author      Mariusz Jakubowski mariusz@orion.gd
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Nexus : MonoBehaviour {

    public enum        Teams { Red, Blue };

    // Set it in object inspector
    public Teams       team;                   // Available teams                 
    public GameObject  unitPrefab;             // Unit prefab
    public Material    materialForRedUnits;
    public Material    materialForBlueUnits;
    public float       unitPerHour = 360f;     // Frequency production units (per hour)
    public float       unitCost = 1f;          // Cost gold per one unit.

    private GoldMine   goldMine;               // Goldmine gives gold for producion of unit
    private float      startProductionTime;    // Real time start of production
    private float      unitProduced;           // Counter units produced
    private float      shouldbeUnitProduced;   // Calculate number of units should be prodeced

    private Teams      colorOfEnemy;
    private GameObject enemyNexus;             // Main target to destroy
    private string     enemyTag;

    private Damage     damage;                 // Health, armor etc.

    void Start()
    {
        if (team == Teams.Blue) colorOfEnemy = Teams.Red;
        else colorOfEnemy = Teams.Blue;

        startProductionTime = Time.realtimeSinceStartup;

        damage = GetComponent<Damage>();
        goldMine = GameObject.Find(team + " Goldmine").GetComponent<GoldMine>();
    }

    void FixedUpdate()
    {
        shouldbeUnitProduced = (Time.realtimeSinceStartup - startProductionTime) / (3600 / unitPerHour);

        if (shouldbeUnitProduced > unitProduced && Time.timeScale > 0)
        {
            if (goldMine.payment(1))
            {
                CreateNewUnit();
                unitProduced++;
            }
            
        }
    }

    void Update()
    {
        if (damage.health <= 0)
        {
            Text winText = GameObject.Find("WinText").GetComponent<Text>();
            GameObject pauseButton = GameObject.Find("PauseButton");
            Destroy(pauseButton);
            winText.text = colorOfEnemy + " team win";
            Time.timeScale = 0;
        }
    }

    void CreateNewUnit()
    {
        Vector2 xz = Random.insideUnitCircle * 1;

        float xoffset;                                      // Respawn new unit near own Nexus
        if (transform.position.x < 0) xoffset = 3;
        else xoffset = -3;

        Vector3 spawnPosition = new Vector3(xz.x + xoffset, 0.3f, xz.y) + transform.position;
        GameObject unit = Instantiate(unitPrefab, spawnPosition, Quaternion.identity) as GameObject;

        unit.tag = team.ToString();

        Renderer unitRenderer = unit.GetComponent<Renderer>();
        if (team == Teams.Blue) unitRenderer.material = materialForBlueUnits;
        else unitRenderer.material = materialForRedUnits;

        Unit unitScript = unit.GetComponent<Unit>();
        unitScript.enemyTag = colorOfEnemy.ToString();
    }
}
