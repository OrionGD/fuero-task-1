﻿/*
 * @author      Mariusz Jakubowski mariusz@orion.gd
 */

using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour
{
    [HideInInspector]
    public string        enemyTag;          // Set by Nexus

    private GameObject   enemyNexus;        // Main target to destroy
    private NavMeshAgent nma;               // Finding way to main target - Nexus
    private GameObject   currentTarget;     // Enemy unit in range to kill
    private int          reloadGun;         // Reload counter
    private Damage       damage;            // Health, armor etc.


    public void Start()
    {
        nma = GetComponent<NavMeshAgent>();
        damage = GetComponent<Damage>();
        enemyNexus = GameObject.Find(enemyTag + " Nexus");
        reloadGun = 0;                      // 0 = gun is reloaded and ready to shoot;
    }

    void Update()
    {
        nma.SetDestination(enemyNexus.transform.position);
        GameObject[] enemyUnits = GameObject.FindGameObjectsWithTag(enemyTag);
        if (enemyUnits.Length > 0)
        {
            if (FindEnemyInRange(enemyUnits, 2f, out currentTarget))
            {
                nma.Stop();
                if (reloadGun == 0)
                {
                    Shoot();
                    reloadGun = damage.reloadTime;
                }
                reloadGun--;                // reloading only when enemy is in range
            }
            else
            {
                nma.Resume();
            }
        }

        if (damage.health <= 0) Destroy(gameObject);
    }

    bool FindEnemyInRange(GameObject[] _enemyUnits, float _maxRange, out GameObject _target)
    {
        _target = _enemyUnits[0];           // Variable must be assigned, but never used when method return false

        foreach (GameObject target in _enemyUnits)
        {
            float distance = Vector3.Distance(transform.position, target.transform.position);
            if (distance <= _maxRange)
            {
                _target = target;
                return true;
            }
        }
        return false;
    }

    void Shoot()
    {
        if (currentTarget != null)
        {
            Color debugLineColor = Color.red;
            if (tag == Nexus.Teams.Blue.ToString()) debugLineColor = Color.blue;
            Debug.DrawLine(transform.position, currentTarget.transform.position, debugLineColor, 0.05f);

            currentTarget.GetComponent<Damage>().ReceiveDamage(damage.atackDamage);
        }
    }

}
